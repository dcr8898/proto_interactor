# ProtoInteractor

A primitive interactor object that combines the Params behavior of [Hanami Actions](https://github.com/hanami/controller) and the Do notation of [dry-monads](https://dry-rb.org/gems/dry-monads/1.6/do-notation/).

This is an experimental project, not meant for production use.

UPDATE: I have decided to abandon this approach in favor of a module-based gem that can be added to any class.

## Installation

Install the gem and add to the application's Gemfile by executing:

    $ bundle add UPDATE_WITH_YOUR_GEM_NAME_IMMEDIATELY_AFTER_RELEASE_TO_RUBYGEMS_ORG

If bundler is not being used to manage dependencies, install the gem by executing:

    $ gem install UPDATE_WITH_YOUR_GEM_NAME_IMMEDIATELY_AFTER_RELEASE_TO_RUBYGEMS_ORG

## Usage

You can read more here: [https://rossney.net/articles/protointeractor-a-simple-business-object-for-hanami/](https://rossney.net/articles/protointeractor-a-simple-business-object-for-hanami/)

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/dcr8898/proto_interactor. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/dcr8898/proto_interactor/blob/main/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the ProtoInteractor project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/dcr8898/proto_interactor/blob/main/CODE_OF_CONDUCT.md).
