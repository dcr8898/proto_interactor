# frozen_string_literal: true

require "test_helper"

class TestInteractor < Minitest::Test
  class NoParamsInteractor < ProtoInteractor::Interactor
    def handle(_user, params)
      params
    end
  end

  class ParamsInteractor < ProtoInteractor::Interactor
    params { required(:name).filled(:str?) }

    def handle(_user, params)
      params
    end
  end

  class DoResultParamsBaseInteractor < ProtoInteractor::Interactor
    BASE_CLASS = true
  end

  class DoResultParamsInteractor < DoResultParamsBaseInteractor
    params { required(:name).filled(:str?) }

    def handle(_user, params)
      validated_params = yield validate(params)

      Success(validated_params)
    end

    private

    def validate(params)
      if params.valid?
        Success(params.to_h)
      else
        Failure[:invalid_data, params.errors, params.error_messages]
      end
    end
  end

  def setup
    super
    @described_class = ProtoInteractor::Interactor
    @user = "Test Bob"
    @ignored_params = {key: "value"}
    @name_and_email_hash = {name: "Test Bob", email: "bob@test.com"}
    @email_only_hash = {email: "bob@test.com"}
  end

  def test_that_it_responds_to_call
    assert_respond_to(@described_class.new, :call)
  end

  def test_without_params_returns_instance_of_base_params
    subject = NoParamsInteractor.new.call(@user, @ignored_params)

    result = subject.class

    assert_equal(ProtoInteractor::Interactor::BaseParams, result)
  end

  def test_without_params_always_returns_empty_hash
    subject = NoParamsInteractor.new.call(@user, @ignored_params)

    result = subject.to_h

    assert_empty(result)
  end

  def test_without_params_always_returns_false_for_valid?
    subject = NoParamsInteractor.new.call(@user, @ignored_params)

    result = subject.valid?

    refute(result)
  end

  def test_with_params_returns_instance_of_params
    subject = ParamsInteractor.new.call(@user, @name_and_email_hash)

    result = subject.class

    assert_equal(TestInteractor::ParamsInteractor::Params, result)
  end

  def test_with_params_returns_only_permitted_params
    subject = ParamsInteractor.new.call(@user, @name_and_email_hash)

    result = subject.to_h

    assert_equal({name: "Test Bob"}, result)
  end

  def test_with_params_returns_true_for_valid?
    subject = ParamsInteractor.new.call(@user, @name_and_email_hash)

    result = subject.valid?

    assert(result)
  end

  def test_with_invalid_params_returns_false_for_valid?
    subject = ParamsInteractor.new.call(@user, @email_only_hash)

    result = subject.valid?

    refute(result)
  end

  def test_success_returns_success_result
    subject = DoResultParamsInteractor.new

    result = subject.call(@user, @name_and_email_hash)

    assert_instance_of(Dry::Monads::Result::Success, result)
  end

  def test_failure_returns_failure_result
    subject = DoResultParamsInteractor.new

    result = subject.call(@user, @email_only_hash)

    assert_instance_of(Dry::Monads::Result::Failure, result)
  end
end
