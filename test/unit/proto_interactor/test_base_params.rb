# frozen_string_literal: true

require "test_helper"

class TestBaseParams < Minitest::Test
  def setup
    super
    @described_class = ProtoInteractor::Interactor::BaseParams
    @ignored_params = {key: "value"}
  end

  def test_that_it_instantiates_a_frozen_object
    subject = @described_class.new

    result = subject.frozen?

    assert(result)
  end

  def test_raw_returns_submitted_params
    subject = @described_class.new(@ignored_params)

    result = subject.raw

    assert_equal(@ignored_params, result)
  end

  def test_always_returns_empty_params
    subject = @described_class.new(@ignored_params)

    result = subject.to_h

    assert_empty(result)
  end

  def test_to_hash_also_returns_empty_params
    subject = @described_class.new(@ignored_params)

    result = subject.to_hash

    assert_empty(result)
  end

  def test_always_returns_false_for_valid?
    subject = @described_class.new(@ignored_params)

    result = subject.valid?

    refute(result)
  end

  def test_each_returns_empty_enumerator
    subject = @described_class.new(@ignored_params)

    result = subject.each.count

    assert_equal(0, result)
  end

  def test_bracket_access_always_returns_nil
    subject = @described_class.new(@ignored_params)

    result = subject[:key]

    assert_nil(result)
  end

  def test_get_always_returns_nil
    subject = @described_class.new(@ignored_params)

    result = subject.get(:key)

    assert_nil(result)
  end

  def test_dig_always_returns_nil
    subject = @described_class.new(@ignored_params)

    result = subject.dig(:key)

    assert_nil(result)
  end
end
