# frozen_string_literal: true

require "test_helper"

class TestParams < Minitest::Test
  def setup
    super
    @described_class = Class.new(ProtoInteractor::Interactor::Params) { params { required(:name).filled(:str?) } }
    @name_and_email_hash = {name: "Test Bob", email: "bob@test.com"}
    @email_only_hash = {email: "bob@test.com"}
  end

  def test_that_it_instantiates_a_frozen_object
    empty_hash = {}
    subject = @described_class.new(empty_hash)

    result = subject.frozen?

    assert(result)
  end

  def test_raw_returns_submitted_params
    subject = @described_class.new(@name_and_email_hash)

    result = subject.raw

    assert_equal(@name_and_email_hash, result)
  end

  def test_returns_only_permitted_params
    subject = @described_class.new(@name_and_email_hash)

    result = subject.to_h

    assert_equal({name: "Test Bob"}, result)
  end

  def test_does_not_return_invalid_params
    subject = @described_class.new(@name_and_email_hash)

    result = subject[:email]

    assert_nil(result)
  end

  def test_to_hash_also_returns_empty_params
    subject = @described_class.new(@name_and_email_hash)

    result = subject.to_hash

    assert_equal({name: "Test Bob"}, result)
  end

  def test_valid_returns_true_for_valid_params
    subject = @described_class.new(@name_and_email_hash)

    result = subject.valid?

    assert(result)
  end

  def test_valid_returns_false_for_invalid_params
    subject = @described_class.new(@email_only_hash)

    result = subject.valid?

    refute(result)
  end

  def test_invalid_params_returns_structured_errors
    subject = @described_class.new(@email_only_hash)

    result = subject.errors

    refute_empty(result)
  end

  def test_invalid_params_returns_array_of_error_messages
    subject = @described_class.new(@email_only_hash)

    result = subject.error_messages

    refute_empty(result)
  end

  def test_each_returns_enumerator
    subject = @described_class.new(@name_and_email_hash)

    result = subject.each

    assert_instance_of(Enumerator, result)
    assert_equal(1, result.count)
    assert_equal([:name, "Test Bob"], result.first)
  end

  def test_bracket_access_gives_access_to_allowed_keys
    subject = @described_class.new(@name_and_email_hash)

    result = subject[:name]

    assert_equal("Test Bob", result)
  end

  def test_get_also_gives_access_to_allowed_keys
    subject = @described_class.new(@name_and_email_hash)

    result = subject.get(:name)

    assert_equal("Test Bob", result)
  end

  def test_dig_also_gives_access_to_allows_keys
    subject = @described_class.new(@name_and_email_hash)

    result = subject.dig(:name)

    assert_equal("Test Bob", result)
  end
end
