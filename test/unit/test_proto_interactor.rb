# frozen_string_literal: true

require "test_helper"

class TestProtoInteractor < Minitest::Test
  def test_that_it_returns_current_version_number
    assert_equal("0.1.0", ProtoInteractor::VERSION)
  end
end
