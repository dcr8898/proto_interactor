# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path("../lib", __dir__)
require "proto_interactor"

require "minitest/autorun"
require "minitest/rg"
