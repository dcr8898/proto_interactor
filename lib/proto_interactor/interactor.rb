# frozen_string_literal: true

require "hanami/utils/string"
require "dry/monads"
require "dry/validation"
require "zeitwerk"

module ProtoInteractor
  class Interactor
    include Dry::Monads[:result, :maybe]

    # A business logic procedural object for use with Hanami.
    # Modeled after (okay, copied from) Hanami::Action.
    # Provides the same "params" functionality as Hanami::Action.
    # Incorporates dry-monads "do notation" to generate result monad.
    #
    # @example
    #   class RegisterUser < Exint::Interactor
    #     params do
    #       required(:name).filled(:string)
    #       required(:email).filled(:string)
    #     end
    #
    #     def handle(user, params)
    #       if params.valid?
    #         # ...
    #       else
    #         # ...
    #       end
    #     end
    #   end

    def self.gem_loader
      @gem_loader ||= Zeitwerk::Loader.new.tap do |loader|
        root = File.expand_path("..", __dir__)
        loader.tag = "proto_interactor"
        loader.inflector = Zeitwerk::GemInflector.new("#{root}/proto_interactor.rb")
        loader.push_dir(root)
        loader.ignore(
          "#{root}/proto_interactor.rb",
          "#{root}/proto_interactor/version.rb"
        )
      end
    end

    gem_loader.setup

    PARAMS_CLASS_NAME = "Params"

    def self.call(...)
      new.call(...)
    end

    def self.inherited(subclass)
      super

      superklass = subclass.superclass

      if superklass.const_defined?(:BASE_CLASS, false) && superklass.const_get(:BASE_CLASS, false) == true
        subclass.class_eval { include Dry::Monads::Do.for(:handle) }
      end
    end

    def self.params_class
      @params_class ||= BaseParams
    end

    def self.params(klass = nil, &blk)
      if klass.nil?
        klass = const_set(PARAMS_CLASS_NAME, Class.new(Params))
        klass.class_eval { params(&blk) }
      end

      @params_class = klass
    end

    def call(user, params = {})
      parsed_params = self.class.params_class.new(params.to_h)

      handle(user, parsed_params)
    end

    protected

    def handle(user, params)
    end
  end
end
