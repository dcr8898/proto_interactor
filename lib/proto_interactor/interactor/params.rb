# frozen_string_literal: true

module ProtoInteractor
  class Interactor
    class Params < BaseParams
      class Errors < SimpleDelegator
        def initialize(errors = {})
          super(errors.dup)
        end

        def add(*args)
          *keys, key, error = args
          _nested_attribute(keys, key) << error
        rescue TypeError
          raise ArgumentError.new("Can't add #{args.map(&:inspect).join(", ")} to #{inspect}")
        end

        private

        def _nested_attribute(keys, key)
          if keys.empty?
            self
          else
            keys.inject(self) { |result, k| result[k] ||= {} }
            dig(*keys)
          end[key] ||= []
        end
      end

      def self.params(&blk)
        validations(&blk || -> {})
      end

      def self.validations(&blk)
        @_validator = Dry::Validation::Contract.build { schema(&blk) }
      end

      def self._validator
        @_validator
      end

      attr_reader :errors

      def initialize(raw_params)
        @raw = raw_params
        validation = validate
        @params = validation.to_h
        @errors = Errors.new(validation.errors.to_h)
        freeze
      end

      def error_messages(error_set = errors)
        error_set.each_with_object([]) do |(key, messages), result|
          k = Hanami::Utils::String.titleize(key)

          msgs = if messages.is_a?(::Hash)
            error_messages(messages)
          else
            messages.map { |message| "#{k} #{message}" }
          end

          result.concat(msgs)
        end
      end

      def validate
        self.class._validator.call(@raw)
      end

      def valid?
        @errors.empty?
      end
    end
  end
end
