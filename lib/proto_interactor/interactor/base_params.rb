# frozen_string_literal: true

module ProtoInteractor
  class Interactor
    # Based on Hanami::Action::BaseParams
    # https://github.com/hanami/controller/blob/main/lib/hanami/action/base_params.rb
    #
    # Where the Hanami version is permissive, this one is restrictive. This class is used by default when
    # no validation schema is defined.
    #
    # When used, @params is always empty ({}) and #valid? is always false.
    #
    # The oririginally submitted params are still available as #raw.
    class BaseParams
      attr_reader :raw

      def initialize(disregarded_params = {})
        @raw = disregarded_params
        @params = {}
        freeze
      end

      def [](key)
        @params[key]
      end

      def each(&blk)
        to_h.each(&blk)
      end

      def get(*keys)
        @params.dig(*keys)
      end
      alias_method :dig, :get

      def to_h
        @params
      end
      alias_method :to_hash, :to_h

      def valid?
        false
      end
    end
  end
end
