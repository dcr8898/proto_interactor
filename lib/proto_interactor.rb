# frozen_string_literal: true

require_relative "proto_interactor/version"
require_relative "proto_interactor/interactor"

module ProtoInteractor
  class Error < StandardError; end
end
