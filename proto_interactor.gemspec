# frozen_string_literal: true

require_relative "lib/proto_interactor/version"

Gem::Specification.new do |spec|
  spec.name = "proto_interactor"
  spec.version = ProtoInteractor::VERSION
  spec.authors = ["Damian C. Rossney"]
  spec.email = ["damian@rossney.net"]

  spec.summary = "A primitive/experimental interactor object for Hanami."
  spec.description = <<~DESC
    ProtoInteractor is a primitive/experimental interactor object created for use in Hanami projects.
    ProtoInteractor combines the Params DSL of Hanami Actions with the Do notation of dry-monads.
  DESC
  spec.homepage = "https://gitlab.com/dcr8898/proto_interactor"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 3.0.0"

  spec.metadata["allowed_push_host"] = ""

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/dcr8898/proto_interactor"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) ||
        f.start_with?(*%w[bin/ test/ spec/ features/ .git appveyor Gemfile])
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "dry-monads", "~> 1.6"
  spec.add_dependency "dry-validation", "~> 1.10"
  spec.add_dependency "hanami-utils", "~> 2.1"
end
